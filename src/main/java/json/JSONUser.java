package json;

import model.Candy;
import java.io.File;
import java.util.List;

public class JSONUser {
  public static void main(String[] args) {
    File json = new File("src/main/resources/candyJSON.json");
    File schema = new File("src/main/resources/candyJSONScheme.json");
    JSONParser parser = new JSONParser();
    printList(parser.getBeerList(json));
  }

  private static void printList(List<Candy> candies) {
    for (Candy candy : candies) {
      System.out.println(candy);
    }
  }
}
