package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Candy;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

class JSONParser {
    private ObjectMapper objectMapper;

    JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    List<Candy> getBeerList(File jsonFile) {
        Candy[] candies = new Candy[0];
        try {
            candies = objectMapper.readValue(jsonFile, Candy[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(candies);
    }
}
